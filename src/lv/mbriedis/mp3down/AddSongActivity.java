package lv.mbriedis.mp3down;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import lv.mbriedis.mp3down.db.DatabaseHelper;
import lv.mbriedis.mp3down.db.Playlist;
import lv.mbriedis.mp3down.db.Song;

import java.sql.SQLException;

public class AddSongActivity extends Activity {
    public static String TAG = "APP";
    private DatabaseHelper dbHelper;

    private Playlist playlist;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.song_add);

        // On button click save
        findViewById(R.id.buttonAddSong).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                saveSong();
            }
        });

        // On enter save song
        ((TextView) findViewById(R.id.inputAddSongName)).setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    saveSong();

                    // Close keyboard
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(findViewById(R.id.inputAddSongName).getWindowToken(), 0);

                    return true;
                } else {
                    return false;
                }
            }
        });

        dbHelper = new DatabaseHelper(getApplicationContext());

        // Load playlist from intent
        int playlistId = getIntent().getIntExtra("playlistId", 0);
        if (playlistId > 0) {
            try {
                playlist = dbHelper.getPlaylistDao().queryForId(playlistId);
            } catch (SQLException e) {
                e.printStackTrace();
                Log.e(TAG, "Failed to get playlist: " + playlistId);
            }
        }

        setTitle(R.string.addNewSongTitle);
    }

    private void saveSong() {
        final EditText text = (EditText) findViewById(R.id.inputAddSongName);
        try {
            Song song = new Song();

            song.setName(text.getText().toString());
            song.setPlaylist(playlist);

            dbHelper.getSongDao().create(song);

            Toast.makeText(getApplicationContext(), "Song added: " + song.getName(), Toast.LENGTH_SHORT).show();

        } catch (SQLException e) {
            Log.i(TAG, e.getMessage());
        }

        Intent data = new Intent();
        if (getParent() == null) {
            setResult(Activity.RESULT_OK, data);
        } else {
            getParent().setResult(Activity.RESULT_OK, data);
        }
        finish();
    }

    @Override
    protected void onStop() {
        super.onStop();
        Player.stop();
    }
}
