package lv.mbriedis.mp3down;

import android.app.AlertDialog;
import android.app.ListActivity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.*;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;
import lv.mbriedis.mp3down.db.DatabaseHelper;
import lv.mbriedis.mp3down.db.Playlist;
import lv.mbriedis.mp3down.db.Song;

import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.*;

public class PlaylistActivity extends ListActivity {
    private static String TAG = "APP";
    private PlaylistArrayAdapter adapter;
    private Context context;
    private DatabaseHelper dbHelper;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        context = getApplicationContext();

        dbHelper = new DatabaseHelper(context);

        setTitle(R.string.playlistTitle);

        loadListItems();
    }

    @Override
    protected void onListItemClick(ListView l, View v, int position, long id) {
        Playlist playlist = adapter.getItem(position);
        Intent intent = new Intent(context, SongListActivity.class);
        intent.putExtra("playlistId", playlist.getId());
        startActivity(intent);
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.playlist, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.newPlaylist:
                newPlaylist();
                return true;
            case R.id.newPlaylistFromYoutube:
                importFromYoutube();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void loadListItems() {
        List<Playlist> playlists = new ArrayList<Playlist>();
        try {
            playlists = dbHelper.getPlaylistDao().queryForAll();
            Collections.reverse(playlists);
        } catch (SQLException e) {
            Log.i(TAG, e.getMessage());
        }

        if (adapter != null && playlists.size() != adapter.getCount()) {
            adapter = null;
        }

        if (adapter == null) {
            adapter = new PlaylistArrayAdapter(context, playlists);
            setListAdapter(adapter);
        } else {
            adapter.setValues(playlists);
            adapter.notifyDataSetChanged();
        }
    }

    private void newPlaylist() {
        LayoutInflater li = LayoutInflater.from(this);
        final View newView = li.inflate(R.layout.playlist_add_dialog, null);

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setView(newView);

        alertDialogBuilder
                .setCancelable(false)
                .setPositiveButton("Add",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {

                                String playlistName = ((EditText) newView.findViewById(R.id.editTextPlaylistName)).getText().toString();

                                try {
                                    Playlist p = new Playlist();
                                    p.setName(playlistName);
                                    dbHelper.getPlaylistDao().create(p);
                                    Toast.makeText(context, "Playlist added: " + p.getName(), Toast.LENGTH_LONG);
                                } catch (SQLException e) {
                                    Log.e(TAG, e.toString());
                                    e.printStackTrace();
                                    Toast.makeText(context, "Failed to add playlist: " + playlistName, Toast.LENGTH_LONG);
                                }
                                // Refresh list
                                loadListItems();
                            }
                        })
                .setNegativeButton("Cancel",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

    private void importFromYoutube() {
        LayoutInflater li = LayoutInflater.from(this);
        final View newView = li.inflate(R.layout.youtube_dialog, null);
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setView(newView);

        alertDialogBuilder
                .setCancelable(false)
                .setPositiveButton("Import",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {

                                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
                                String playlistName = "YT (" + sdf.format(new Date()) + ")";

                                Playlist tmpPlaylist = new Playlist();
                                tmpPlaylist.setName(playlistName);

                                try {
                                    dbHelper.getPlaylistDao().create(tmpPlaylist);
                                } catch (SQLException e) {
                                    Log.e(TAG, e.toString());
                                    e.printStackTrace();
                                    Toast.makeText(context, "Failed to add playlist: " + playlistName, Toast.LENGTH_LONG);
                                }

                                final Playlist playlist = tmpPlaylist;

                                // Refresh list
                                loadListItems();

                                final YouTubeImporter yt = new YouTubeImporter(((EditText) newView.findViewById(R.id.editTextYouTubePlaylist)).getText().toString());
                                yt.start(new Runnable() {
                                    @Override
                                    public void run() {
                                        Log.i(TAG, "YouTube finished");
                                        Log.i(TAG, yt.getItems().toString());
                                        for (String s : yt.getItems()) {
                                            Song song = new Song();
                                            song.setName(s);
                                            song.setPlaylist(playlist);
                                            try {
                                                dbHelper.getSongDao().create(song);
                                            } catch (SQLException e) {
                                                e.printStackTrace();
                                                Log.e(TAG, "Insert failed: " + e.toString());
                                            }
                                        }
                                    }
                                });
                            }
                        })
                .setNegativeButton("Cancel",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }
}