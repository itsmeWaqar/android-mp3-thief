package lv.mbriedis.mp3down.db;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.field.ForeignCollectionField;
import com.j256.ormlite.table.DatabaseTable;

import java.util.Collection;

@DatabaseTable(tableName = "songs")
public class Song {

    @DatabaseField
    private String name;
    @DatabaseField
    private String nameFormatted;

    @ForeignCollectionField(eager = true)
    Collection<SongUrl> urls;

    @DatabaseField(canBeNull = false, foreign = true, foreignAutoRefresh = true)
    private Playlist playlist;

    public Playlist getPlaylist() {
        return playlist;
    }

    public void setPlaylist(Playlist playlist) {
        this.playlist = playlist;
    }

    public Song() {
        // ORMLite needs a no-arg constructor
    }

    @DatabaseField(generatedId = true)
    private int id;

    public void setId(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
        formatName();
    }

    private void formatName() {
        this.nameFormatted = this.name.toLowerCase();
        // TODO parse the name
    }

    public String getNameFormatted() {
        return this.nameFormatted;
    }

    public Collection<SongUrl> getUrls() {
        return urls;
    }
}
