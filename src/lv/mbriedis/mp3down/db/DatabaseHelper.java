package lv.mbriedis.mp3down.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;
import com.j256.ormlite.android.apptools.OrmLiteSqliteOpenHelper;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;
import lv.mbriedis.mp3down.R;

import java.sql.SQLException;

public class DatabaseHelper extends OrmLiteSqliteOpenHelper {

    private Dao<Song, Integer> songDao = null;
    private Dao<SongUrl, Integer> songUrlDao = null;
    private Dao<Playlist, Integer> playlistDao = null;

    public DatabaseHelper(Context context) {
        super(context, Config.DB_NAME, null, Config.DB_VERSION, R.raw.ormlite);
    }

    @Override
    public void onCreate(SQLiteDatabase db, ConnectionSource connectionSource) {
        try {
            Log.i(DatabaseHelper.class.getName(), "onCreate");

            TableUtils.createTable(connectionSource, Song.class);
            TableUtils.createTable(connectionSource, SongUrl.class);
            TableUtils.createTable(connectionSource, Playlist.class);

        } catch (SQLException e) {
            Log.e(DatabaseHelper.class.getName(), "Can't create database", e);
            throw new RuntimeException(e);
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, ConnectionSource connectionSource, int oldVersion, int newVersion) {
        try {
            Log.i(DatabaseHelper.class.getName(), "onUpgrade");

            TableUtils.dropTable(connectionSource, Song.class, true);
            TableUtils.dropTable(connectionSource, SongUrl.class, true);
            TableUtils.dropTable(connectionSource, Playlist.class, true);

            // after we drop the old databases, we create the new ones
            onCreate(db, connectionSource);
        } catch (SQLException e) {
            Log.e(DatabaseHelper.class.getName(), "Can't drop databases", e);
            throw new RuntimeException(e);
        }
    }

    public Dao<Song, Integer> getSongDao() throws SQLException {
        if (songDao == null) {
            songDao = getDao(Song.class);
        }
        return songDao;
    }

    public Dao<Playlist, Integer> getPlaylistDao() throws SQLException {
        if (playlistDao == null) {
            playlistDao = getDao(Playlist.class);
        }
        return playlistDao;
    }

    public Dao<SongUrl, Integer> getSongUrlDao() throws SQLException {
        if (songUrlDao == null) {
            songUrlDao = getDao(SongUrl.class);
        }
        return songUrlDao;
    }
}
