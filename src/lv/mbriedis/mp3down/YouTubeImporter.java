package lv.mbriedis.mp3down;

import android.os.AsyncTask;
import android.util.Log;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class YouTubeImporter {
    private String playlist;
    private Runnable callback;
    private static String TAG = "APP";
    private List<String> items = new ArrayList<String>();

    public YouTubeImporter(String youTubePlaylist) {
        playlist = youTubePlaylist.toUpperCase();
    }

    private String getUrl(int page) {
        int perPage = 50;
        return "http://gdata.youtube.com/feeds/api/playlists/" + playlist + "?v=2&alt=json&feature=plcp" +
                "&max-results=" + perPage +
                "&start-index=" + (perPage * (page - 1) + 1) +
                "&fields=entry(title)&prettyprint=true";
    }

    private List<String> resolvePlaylistItems(String html) {
        Log.i(TAG, html);

        ArrayList<String> result = new ArrayList<String>();

        JSONObject jo = null;
        try {
            jo = new JSONObject(html);
        } catch (JSONException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            Log.e(TAG, "Failed to parse JSON: " + e.toString());
        }

        if (jo == null) {
            return result;
        }

        JSONArray entries = new JSONArray();

        try {
            entries = jo.getJSONObject("feed").getJSONArray("entry");
        } catch (JSONException e) {
            e.printStackTrace();
            Log.e(TAG, "Failed entry getting:" + e.toString());
        }

        for (int i = 0; i < entries.length(); i++) {
            String item = null;
            try {
                item = (String) ((JSONObject) ((JSONObject) entries.get(i)).get("title")).get("$t");
                if (item != null) {
                    result.add(item);
                }
            } catch (JSONException e) {
                e.printStackTrace();
                Log.e(TAG, "Failed: " + e.toString());
            }

        }

        return result;
    }

    public void start(Runnable callback) {
        this.callback = callback;
        new MainTask().execute("");
    }

    /**
     * Task that fetches html and sends it to URL parsing
     */
    private class MainTask extends AsyncTask<String, Integer, List<String>> {
        protected List<String> doInBackground(String[] playlistId) {
            Log.i(TAG, "Get YouTube playlist: " + Thread.currentThread().getName());
            List<String> items = new ArrayList<String>();
            for (int i = 1; i < 10; i++) {
                Log.i(TAG, "Fetching page: " + i);
                String html = Utils.getHtml(getUrl(i));
                List<String> tmpItems = resolvePlaylistItems(html);
                if (tmpItems.size() == 0) {
                    Log.i(TAG, "No items found, breaking");
                    break;
                } else {
                    Log.i(TAG, "Items fetched: " + tmpItems.size());
                    items.addAll(tmpItems);
                }
            }
            return items;
        }

        protected void onPostExecute(List<String> resolvedItems) {
            items = resolvedItems;
            if (callback != null) {
                callback.run();
            }
        }
    }

    public List<String> getItems() {
        return items;
    }
}
