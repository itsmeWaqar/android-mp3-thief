package lv.mbriedis.mp3down;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.SeekBar;
import android.widget.TextView;
import lv.mbriedis.mp3down.db.DatabaseHelper;
import lv.mbriedis.mp3down.db.SongUrl;

import java.sql.SQLException;

public class StreamActivity extends Activity {

    private SongUrl songUrl;
    private final int PLAYER_IDLE = 1;
    private final int PLAYER_LOADING = 2;
    private final int PLAYER_PLAYING = 3;

    private final Handler handler = new Handler();


    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.song_stream);
        getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);

        SongUrl tmpSongUrl = null;

        int songUrlId = getIntent().getIntExtra("songUrlId", 0);

        DatabaseHelper dbHelper = new DatabaseHelper(getApplicationContext());

        try {
            tmpSongUrl = dbHelper.getSongUrlDao().queryForId(songUrlId);
        } catch (SQLException e) {
            e.printStackTrace();
        }

        if (tmpSongUrl == null) {
            finish();
            return;
        }

        final SongUrl songUrl = tmpSongUrl;

        TextView title = (TextView) findViewById(R.id.textViewSongTitle);
        title.setText(songUrl.getTitle());

        Button button = (Button) findViewById(R.id.buttonStreamPlay);
        button.setText(R.string.streamStart);
        button.setOnClickListener(streamOnClickListener);

        SeekBar seekBar = (SeekBar) findViewById(R.id.seekBarSongProgress);
        seekBar.setEnabled(false);
        seekBar.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                MediaPlayer mp = Player.getMediaPlayer();
                if (mp != null && mp.isPlaying()) {
                    SeekBar sb = (SeekBar) v;
                    mp.seekTo(sb.getProgress());
                }
                return false;
            }
        });

        findViewById(R.id.buttonDownload).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LayoutInflater li = LayoutInflater.from(StreamActivity.this);
                final View downloadView = li.inflate(R.layout.download_prompt, null);

                ((EditText) downloadView.findViewById(R.id.editTextDownloadFilename)).setText(songUrl.getTitle());

                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(StreamActivity.this);
                alertDialogBuilder.setView(downloadView);
                alertDialogBuilder.setTitle(R.string.downloadFilename);

                alertDialogBuilder
                        .setPositiveButton(R.string.accept,
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        String filename = ((EditText) downloadView.findViewById(R.id.editTextDownloadFilename)).getText().toString();
                                        new SongDownloader(songUrl, filename, StreamActivity.this).start();
                                    }
                                })
                        .setNegativeButton(R.string.cancel,
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        dialog.cancel();
                                    }
                                });
                AlertDialog alertDialog = alertDialogBuilder.create();
                alertDialog.show();
            }
        });

        this.setTitle(R.string.view_song);
        this.songUrl = songUrl;
    }

    private View.OnClickListener streamOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(final View v) {
            final Button button = (Button) v;
            final SeekBar seekBar = (SeekBar) findViewById(R.id.seekBarSongProgress);

            seekBar.setEnabled(true);

            // If is playing or loading, stop and return
            if (v.getTag() != null && !v.getTag().equals(PLAYER_IDLE)) {
                Player.stop();
                seekBar.setEnabled(false);
                v.setTag(PLAYER_IDLE);
                button.setText(R.string.streamStart);
                return;
            }

            v.setTag(PLAYER_LOADING);
            button.setText(R.string.streamLoading);

            Player.initialize(songUrl.getUrl(), getApplicationContext());

            Player.getMediaPlayer().setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                private SeekBar _seekBar = null;
                private MediaPlayer _mp = null;

                @Override
                public void onPrepared(MediaPlayer mp) {
                    this._seekBar = seekBar;
                    this._mp = mp;

                    button.setText(R.string.stream_playing);
                    v.setTag(PLAYER_PLAYING);

                    seekBar.setMax(mp.getDuration());
                    seekBar.setEnabled(true);

                    mp.seekTo(seekBar.getProgress());
                    mp.start();

                    mp.setOnBufferingUpdateListener(new MediaPlayer.OnBufferingUpdateListener() {
                        @Override
                        public void onBufferingUpdate(MediaPlayer mp, int percent) {
                            seekBar.setSecondaryProgress((seekBar.getMax() / 100) * percent);
                        }
                    });
                    primarySeekBarProgressUpdater();
                }

                private void primarySeekBarProgressUpdater() {
                    if (_seekBar == null || _mp == null) {
                        return;
                    }

                    try {
                        _seekBar.setProgress(_mp.getCurrentPosition());
                    } catch (IllegalStateException e) {
                        e.printStackTrace();
                        return;
                    }

                    if (_mp.isPlaying()) {
                        Runnable notification = new Runnable() {
                            public void run() {
                                primarySeekBarProgressUpdater();
                            }
                        };
                        handler.postDelayed(notification, 1000);
                    }
                }
            });
            Player.play();
        }
    };

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        Player.stop();
    }

    @Override
    protected void onStop() {
        super.onStop();
        Player.stop();
    }
}