package lv.mbriedis.mp3down;


import android.content.Context;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.util.Log;

import java.io.IOException;
import java.util.concurrent.Callable;

public class Player {
    private static String TAG = "APP";
    private static MediaPlayer _mediaPlayer = null;


    public static void initialize(String url, Context context) {
        if (_mediaPlayer != null) {
            stop();
        }

        _mediaPlayer = new MediaPlayer();
        _mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);

        try {
            _mediaPlayer.setDataSource(context, Uri.parse(url));
        } catch (IOException e) {
            e.printStackTrace();
            Log.e(TAG, "Couldn't set data source! Message: " + e.getMessage());
        }
    }

    public static void play() {
        if (_mediaPlayer != null) {
            try {
                _mediaPlayer.prepareAsync();
            } catch (Exception e) {
                e.printStackTrace();
                Log.e(TAG, "Couldn't prepare! Message: " + e.getMessage() + " " + e.getClass().getName());
            }
        }
    }

    public static void stop() {
        if (_mediaPlayer != null) {
            _mediaPlayer.stop();
            _mediaPlayer.release();
            _mediaPlayer = null;
        }
    }

    public static MediaPlayer getMediaPlayer() {
        return _mediaPlayer;
    }
}
