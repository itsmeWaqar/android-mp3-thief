package lv.mbriedis.mp3down;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import lv.mbriedis.mp3down.db.DatabaseHelper;
import lv.mbriedis.mp3down.db.Song;

import java.sql.SQLException;

public class ReceiveIntentActivity extends Activity {
    private static String APP = "APP";

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Get intent, action and MIME type
        Intent intent = getIntent();
        String action = intent.getAction();
        String type = intent.getType();

        if (Intent.ACTION_SEND.equals(action) && type != null) {
            if ("text/plain".equals(type)) {
                handleSendText(intent); // Handle text being sent
            }
        }
    }

    private void handleSendText(Intent intent) {
        String text = intent.getStringExtra(Intent.EXTRA_TEXT);
        String subject = intent.getStringExtra(Intent.EXTRA_SUBJECT);

        // Shazam puts song name within the subject extra data
        if (subject != null && text.toLowerCase().contains("shazam")) {
            text = subject;
        }


        if (text == null) {
            Intent newIntent = new Intent(getApplicationContext(), AddSongActivity.class);
            startActivity(newIntent);
            finish();
        }

        // Song is given, lets add to db
        DatabaseHelper dbHelper = new DatabaseHelper(getApplicationContext());

        Song song = new Song();
        song.setName(text);

        try {
            dbHelper.getSongDao().create(song);
        } catch (SQLException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            Log.e(APP, "Failed to add song!");
            return;
        }

        if(song.getId() > 0){
            Intent newIntent = new Intent(getApplicationContext(), SongUrlListActivity.class);
            newIntent.putExtra("songId", song.getId());
            startActivity(newIntent);
            finish();
        }
    }
}